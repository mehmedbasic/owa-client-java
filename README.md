# owa-client-java - A client for Open Web Analytics written in Java

The purpose of the project is to enable web analytics in projects where JavaScript and/or PHP are not an option.

The project has no affiliation with the authors of Open Web Analytics.

The project is licensed under the GNU Lesser General Public License v2.1 (LGPLv2.1).
For more information consult the LICENSE file or [online](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt) (gnu.org).

## Gettng started

To get started with this project you will need:

- A working Open Web Analytics installation with a site id.
- A jar of `owa-client-java` on your classpath.
- Java 8
- An indefinite amount of coffee.

````java
URL logUrl = new URL("https://baseurl.com/owa/log.php");
String siteId = "your_very_long_site_id";
TrackerConfiguration configuration = new TrackerConfiguration(logUrl, siteId);
Tracker tracker = new Tracker(configuration);

RequestInfo info = // create RequestInfo based on your application needs

// Track an action:
tracker.newAction("Group", info)
        .name("Name of action")
        .label("Label")
        .numericValue(42).done();
        
// Track a page request:
tracker.trackEvent(EventTypes.Base.PageRequest, info);        
````

And voila! You have successfully added analytics to your application.
