package dk.mehmedbasic.owabindings.tracking;

import dk.mehmedbasic.owabindings.ConstantWithValue;
import dk.mehmedbasic.owabindings.event.ActionInfo;

import java.util.function.Consumer;

/**
 * Fluent builder for action classes.
 */
class ActionBuilder {
    static ActionBuilder start(ConstantWithValue group, Consumer<ActionInfo> callback) {
        return new ActionBuilder(group.getValue(), callback);
    }

    static ActionBuilder start(String group, Consumer<ActionInfo> callback) {
        return new ActionBuilder(group, callback);
    }

    private String group;
    private String name;
    private String label;
    private Integer numericValue;

    private final Consumer<ActionInfo> callback;

    private ActionBuilder(String group, Consumer<ActionInfo> callback) {
        this.group = group;
        this.callback = callback;
    }

    ActionBuilder name(ConstantWithValue name) {
        return name(name.getValue());
    }

    ActionBuilder name(String name) {
        this.name = name;
        return this;
    }

    ActionBuilder label(ConstantWithValue label) {
        return label(label.getValue());
    }

    ActionBuilder label(String label) {
        this.label = label;
        return this;
    }

    ActionBuilder numericValue(int value) {
        this.numericValue = value;
        return this;
    }

    void done() {
        callback.accept(new ActionInfo(group, name, label, numericValue));
    }
}
