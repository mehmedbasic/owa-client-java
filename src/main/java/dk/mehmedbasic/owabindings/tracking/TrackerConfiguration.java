package dk.mehmedbasic.owabindings.tracking;

import java.net.URL;

public class TrackerConfiguration {
    private final URL baseUrl;
    private final String siteId;

    public TrackerConfiguration(URL baseUrl, String siteId) {
        this.baseUrl = baseUrl;
        this.siteId = siteId;
    }

    public URL getBaseUrl() {
        return baseUrl;
    }

    public String getSiteId() {
        return siteId;
    }
}
