package dk.mehmedbasic.owabindings.tracking;

import dk.mehmedbasic.owabindings.ConstantWithValue;
import dk.mehmedbasic.owabindings.event.*;
import dk.mehmedbasic.owabindings.server.EventLogger;
import dk.mehmedbasic.owabindings.server.ExecutorBasedLogger;

import java.util.function.Consumer;


public final class Tracker {
    private final TrackerConfiguration configuration;
    private final EventLogger eventLogger;
    private Consumer<ActionInfo> loggerConsumer;

    public Tracker(TrackerConfiguration configuration) {
        this(configuration, ExecutorBasedLogger.newSingleThreadedRequestHandler());
    }

    public Tracker(TrackerConfiguration configuration, EventLogger eventLogger) {
        this.configuration = configuration;
        this.eventLogger = eventLogger;
    }

    public ActionBuilder newAction(ConstantWithValue group, RequestInfo requestInfo) {
        return newAction(group.getValue(), requestInfo);
    }

    public ActionBuilder newAction(String group, RequestInfo requestInfo) {
        loggerConsumer = (action) -> {
            Event event = new Event(EventTypes.Track.Action, configuration);
            event.setRequestInfo(requestInfo);
            event.addAdditionalInfo(new SiteId(configuration.getSiteId()));
            event.addAdditionalInfo(action);

            eventLogger.logEvent(event);
        };
        return ActionBuilder.start(group, loggerConsumer);
    }

    public void trackEvent(EventType eventType, RequestInfo requestInfo) {
        Event event = new Event(eventType, configuration);
        event.setRequestInfo(requestInfo);
        event.addAdditionalInfo(new SiteId(configuration.getSiteId()));

        eventLogger.logEvent(event);
    }
}
