package dk.mehmedbasic.owabindings.event;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class RequestInfo implements ParameterObject {
    @NamedParam("owa_session_id")
    private final long sessionId;

    @NamedParam("owa_visitor_id")
    private final long userId;

    @NamedParam("owa_page_title")
    private String pageTitle;

    @NamedParam("owa_page_url")
    private String pageUrl;

    private final String userAgent;
    private final String referer;

    public RequestInfo(long sessionId, long userId, String userAgent, String referer) {
        this.sessionId = sessionId;
        this.userId = userId;
        this.userAgent = userAgent;
        this.referer = referer;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public String getReferer() {
        return referer;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }
}
