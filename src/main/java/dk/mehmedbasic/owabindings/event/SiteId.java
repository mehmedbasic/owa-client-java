package dk.mehmedbasic.owabindings.event;

public class SiteId extends KeyValuePair {
    public SiteId(String siteId) {
        super("owa_site_id", siteId);
    }
}
