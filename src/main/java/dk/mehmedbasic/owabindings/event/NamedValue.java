package dk.mehmedbasic.owabindings.event;

/**
 * A tagging interface for objects that only hold data.
 */
public interface NamedValue {
    String getName();

    Object getValue();
}