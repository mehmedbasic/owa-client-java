package dk.mehmedbasic.owabindings.event;

import dk.mehmedbasic.owabindings.ConstantWithValue;

public final class EventTypes {
    public enum Base implements EventType, ConstantWithValue {
        PageRequest("base.page_request");

        private String value;

        Base(String value) {
            this.value = value;
        }

        @Override
        public String getValue() {
            return value;
        }
    }

    public enum Track implements EventType, ConstantWithValue {
        Action("track.action");

        String value;

        Track(String value) {
            this.value = value;
        }

        @Override
        public String getValue() {
            return value;
        }
    }

    public enum Dom implements EventType, ConstantWithValue {
        Click("dom.click");

        String value;

        Dom(String value) {
            this.value = value;
        }

        @Override
        public String getValue() {
            return value;
        }
    }
}
