package dk.mehmedbasic.owabindings.event;

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class ActionInfo implements ParameterObject {
    @NamedParam("owa_action_group")
    private final String group;

    @NamedParam("owa_action_name")
    private final String name;

    @NamedParam("owa_action_label")
    private final String label;

    @NamedParam("owa_numeric_value")
    private final Integer numericValue;

    public ActionInfo(String group, String name, String label, Integer numericValue) {
        this.group = group;
        this.name = name;
        this.label = label;
        this.numericValue = numericValue;
    }
}
