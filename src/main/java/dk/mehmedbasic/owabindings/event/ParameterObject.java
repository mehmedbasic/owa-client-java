package dk.mehmedbasic.owabindings.event;

/**
 * A tagging interface for dumb objects that only hold data.
 */
interface ParameterObject {
}