package dk.mehmedbasic.owabindings.event;

public class KeyValuePair implements NamedValue, ParameterObject {
    private final String name;
    private final Object value;

    public KeyValuePair(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final Object getValue() {
        return value;
    }
}
