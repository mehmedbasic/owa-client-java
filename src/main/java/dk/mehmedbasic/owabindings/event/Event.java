package dk.mehmedbasic.owabindings.event;

import dk.mehmedbasic.owabindings.tracking.TrackerConfiguration;

import java.util.LinkedList;
import java.util.List;

public final class Event {
    @SuppressWarnings("unused")
    @NamedParam("owa_event_type")
    private final EventType eventType;

    @SuppressWarnings("unused")
    @NamedParamComposite
    private RequestInfo requestInfo;

    @SuppressWarnings({"unused", "MismatchedQueryAndUpdateOfCollection"})
    @NamedParamComposite
    private final List<ParameterObject> additionalInfo = new LinkedList<>();

    @NamedParam("owa_timestamp")
    private final long timeStamp = System.currentTimeMillis() / 1000L;

    @NamedParam("owa_is_new_visitor")
    private final boolean newVisitor = false;

    private final TrackerConfiguration configuration;

    public Event(EventType eventType, TrackerConfiguration configuration) {
        this.eventType = eventType;
        this.configuration = configuration;
    }

    public void setRequestInfo(RequestInfo requestInfo) {
        this.requestInfo = requestInfo;
    }

    public RequestInfo getRequestInfo() {
        return requestInfo;
    }

    public void addAdditionalInfo(ParameterObject additionalInfo) {
        this.additionalInfo.add(additionalInfo);
    }

    public TrackerConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventType=" + eventType +
                ", requestInfo=" + requestInfo +
                ", additionalInfo=" + additionalInfo +
                ", timeStamp=" + timeStamp +
                ", newVisitor=" + newVisitor +
                ", configuration=" + configuration +
                '}';
    }
}
