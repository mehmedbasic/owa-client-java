package dk.mehmedbasic.owabindings;

/**
 * A named constant interface.
 */
public interface ConstantWithValue {
    String getValue();
}