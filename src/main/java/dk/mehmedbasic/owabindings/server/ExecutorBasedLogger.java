package dk.mehmedbasic.owabindings.server;

import dk.mehmedbasic.owabindings.event.Event;
import dk.mehmedbasic.owabindings.event.RequestInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public final class ExecutorBasedLogger implements EventLogger {
    private static final Logger logger = LoggerFactory.getLogger(ExecutorBasedLogger.class);

    public static ExecutorBasedLogger newSingleThreadedRequestHandler() {
        return new ExecutorBasedLogger(Executors.newSingleThreadExecutor());
    }

    public static ExecutorBasedLogger newMultiThreadRequestHandler(int threads) {
        return new ExecutorBasedLogger(Executors.newFixedThreadPool(threads));
    }

    private final ScheduledExecutorService delayScheduler = Executors.newScheduledThreadPool(2);

    private final Executor executor;
    private static final int maxRetries = 5;

    private ExecutorBasedLogger(Executor executor) {
        this.executor = executor;
    }

    @Override
    public void logEvent(Event event) {
        Consumer<EventLog> callback = log -> delayScheduler.schedule(log, 10, TimeUnit.SECONDS);
        executor.execute(new EventLog(event, callback));
    }

    private static final class EventLog implements Runnable {
        private int run = 0;
        private final Event event;
        private final Consumer<EventLog> retryCallback;

        EventLog(Event event, Consumer<EventLog> retryCallback) {
            this.event = event;
            this.retryCallback = retryCallback;
        }

        @Override
        public void run() {
            try {
                UrlConverter converter = new UrlConverter(event.getConfiguration());
                URL requestUrl = converter.convert(event);
                RequestInfo requestInfo = event.getRequestInfo();
                String agent = requestInfo.getUserAgent();
                String referer = requestInfo.getReferer();

                UrlRequest request = new UrlRequest(requestUrl, agent, referer);
                request.execute();
            } catch (Throwable throwable) {
                if (run < maxRetries) {
                    logger.warn("An error occurred during the request, run number $run, retrying in 10 seconds.", throwable);
                    retryCallback.accept(this);
                } else {
                    logger.warn("An error occurred during the request, max retries exceeded, stopping.", throwable);
                }
                run++;
            }
        }
    }

    private static final class UrlRequest {
        private URL url;
        private String userAgent;
        private String referer;

        private UrlRequest(URL url, String userAgent, String referer) {
            this.url = url;
            this.userAgent = userAgent;
            this.referer = referer;
        }

        void execute() throws IOException {
            logger.debug("Sending request to URL: " + url);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", userAgent);
            connection.setRequestProperty("Referer", referer);

            InputStream stream = connection.getInputStream();
            //noinspection StatementWithEmptyBody
            while (stream.read() != -1) {
            }
            logger.debug("Got response: " + connection.getResponseMessage());
        }
    }
}
