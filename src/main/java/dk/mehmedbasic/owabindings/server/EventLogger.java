package dk.mehmedbasic.owabindings.server;

import dk.mehmedbasic.owabindings.event.Event;

public interface EventLogger {
    void logEvent(Event event);
}
