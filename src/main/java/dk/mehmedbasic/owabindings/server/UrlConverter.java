package dk.mehmedbasic.owabindings.server;

import dk.mehmedbasic.owabindings.ConstantWithValue;
import dk.mehmedbasic.owabindings.event.Event;
import dk.mehmedbasic.owabindings.event.NamedParam;
import dk.mehmedbasic.owabindings.event.NamedParamComposite;
import dk.mehmedbasic.owabindings.event.NamedValue;
import dk.mehmedbasic.owabindings.tracking.TrackerConfiguration;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.*;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public final class UrlConverter {
    private URL baseUrl;

    public UrlConverter(TrackerConfiguration configuration) {
        this.baseUrl = configuration.getBaseUrl();
    }

    public URL convert(Event event) {
        try {
            List<String> urlQueries = readQueries(event).stream()
                    .filter(queryParam -> !"".equals(queryParam.value))
                    .map(queryParam -> queryParam.key + "=" + queryParam.value)
                    .collect(Collectors.toList());

            String joined = String.join("&", urlQueries);
            String spec = baseUrl.toString() + "?" + joined;
            return new URI(spec).toURL();
        } catch (UnsupportedEncodingException | IllegalAccessException | MalformedURLException | URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private static List<QueryParam> readQueries(Object object) throws UnsupportedEncodingException, IllegalAccessException {
        if (object instanceof NamedValue) {
            NamedValue named = (NamedValue) object;
            QueryParam query = new QueryParam(named.getName(), named.getValue());
            return Collections.singletonList(query);
        } else {
            return introspectObject(object);
        }
    }

    private static List<QueryParam> introspectObject(Object object) throws IllegalAccessException, UnsupportedEncodingException {
        List<QueryParam> params = new LinkedList<>();
        for (Field field : object.getClass().getDeclaredFields()) {
            boolean oldAccessible = field.isAccessible();

            field.setAccessible(true);

            NamedParam param = field.getAnnotation(NamedParam.class);
            if (param != null) {
                Object value = field.get(object);
                if (value != null)
                    if (value instanceof ConstantWithValue) {
                        ConstantWithValue constantWithValue = (ConstantWithValue) value;
                        params.add(new QueryParam(param.value(), constantWithValue.getValue()));
                    } else {
                        params.add(new QueryParam(param.value(), value));
                    }
            }

            NamedParamComposite composite = field.getAnnotation(NamedParamComposite.class);
            if (composite != null) {
                Object nestedObject = field.get(object);
                if (nestedObject instanceof Collection) {
                    @SuppressWarnings("unchecked") Collection<Object> collection = (Collection<Object>) nestedObject;
                    for (Object parameterObject : collection) {
                        params.addAll(readQueries(parameterObject));
                    }
                } else {
                    params.addAll(readQueries(nestedObject));
                }
            }

            field.setAccessible(oldAccessible);
        }
        return params;
    }

    private static final class QueryParam {
        String key;
        String value;

        QueryParam(String key, Object payload) throws UnsupportedEncodingException {
            this.key = key;
            this.value = URLEncoder.encode(String.valueOf(payload), "utf-8");
        }
    }
}
