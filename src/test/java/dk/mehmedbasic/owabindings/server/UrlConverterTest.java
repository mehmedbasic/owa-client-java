package dk.mehmedbasic.owabindings.server;

import dk.mehmedbasic.owabindings.event.Event;
import dk.mehmedbasic.owabindings.event.EventTypes;
import dk.mehmedbasic.owabindings.event.RequestInfo;
import dk.mehmedbasic.owabindings.event.SiteId;
import dk.mehmedbasic.owabindings.tracking.TrackerConfiguration;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

public final class UrlConverterTest {
    private UrlConverter converter;
    private TrackerConfiguration trackerConfiguration;

    @Before
    public void setup() throws MalformedURLException {
        trackerConfiguration = new TrackerConfiguration(new URL("http://localhost/log.php"), "site");
        converter = new UrlConverter(trackerConfiguration);
    }

    @Test
    void converterShouldReadFieldsCorrectly() throws Exception {
        RequestInfo info = new RequestInfo(43, 42, "Chrome 42", null);

        Event event = new Event(EventTypes.Base.PageRequest, trackerConfiguration);
        event.setRequestInfo(info);
        event.addAdditionalInfo(new SiteId(trackerConfiguration.getSiteId()));

        String resultUrl = converter.convert(event).toString();
        Assertions.assertThat(resultUrl).startsWith("http://localhost/log.php?");
        Assertions.assertThat(resultUrl).contains("owa_session_id=session");
        Assertions.assertThat(resultUrl).contains("owa_visitor_id=42");
        Assertions.assertThat(resultUrl).contains("owa_site_id=site");
    }
}

