package dk.mehmedbasic.owabindings.tracking;

import dk.mehmedbasic.owabindings.ConstantWithValue;
import dk.mehmedbasic.owabindings.event.EventTypes;
import dk.mehmedbasic.owabindings.event.RequestInfo;
import dk.mehmedbasic.owabindings.server.UrlConverter;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

public final class TrackerTest {
    private Tracker tracker;
    private final TrackerConfiguration configuration
            = new TrackerConfiguration(new URL("https://baseurl.com/owa/log.php"), "a468c03c90182815a235e47e1c18dd3c");

    public TrackerTest() throws MalformedURLException {
    }

    @Before
    public void setup() {
        UrlConverter converter = new UrlConverter(configuration);
        tracker = new Tracker(configuration, event -> System.out.println(converter.convert(event)));
    }

    @Test
    public void loggerShouldLogPageRequest() throws Exception {
        RequestInfo info = new RequestInfo(1473018911705324499L, 1473018911111066445L, "Chrome 42, Windows 7", null);
        info.setPageTitle("Hello World");
        info.setPageUrl("http://stuff.dk/stuff");

        tracker.trackEvent(EventTypes.Base.PageRequest, info);
    }

    @Test
    public void loggerShouldLogAction() throws Exception {
        RequestInfo info = new RequestInfo(1473018911705324499L, 1473018911111066445L, "Chrome 42, Windows 7", null);
        info.setPageTitle("Hello World");
        info.setPageUrl("http://stuff.dk/stuff");

        tracker.newAction(Values.Stuff, info)
                .label(Values.Banana)
                .name(Values.Banana)
                .numericValue(42).done();
    }

    private enum Values implements ConstantWithValue {
        Stuff("stuff"), Banana("banana");

        String value;

        Values(String value) {
            this.value = value;
        }

        @Override
        public String getValue() {
            return value;
        }
    }
}
